/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author sairu
 */
public class testAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Beem","White",0);
        animal.speak();
        animal.walk();
        
        Dog dog1 = new Dog("Dang","Black&White");
        dog1.speak();
        dog1.walk();
        
        Dog dog2 = new Dog("To","Orange");
        dog2.speak();
        dog2.walk();
        
        Dog dog3 = new Dog("Mome","Black&White");
        dog3.speak();
        dog3.walk();
        
        Dog dog4 = new Dog("Bat","Black&White");
        dog4.speak();
        dog4.walk();
        
        Cat cat = new Cat("Zero","Orange");
        cat.speak();
        cat.walk();
        
        Duck duck1 = new Duck("Som","Yellor");
        duck1.speak();
        duck1.walk();
        duck1.fly();
        
        Duck duck2 = new Duck("GabGab","Black");
        duck2.speak();
        duck2.walk();
        duck2.fly();
        
        System.out.println();
        System.out.println("Dang is Animal: "+ (dog1 instanceof Animal));
        System.out.println("Dang is Dog: "+ (dog1 instanceof Dog));
        System.out.println("Dang is Cat:  "+ (dog1 instanceof Object));
        System.out.println("Animal is Dog: "+(animal instanceof Dog));
        System.out.println("Animal is Animal: "+(animal instanceof Animal));
        
        Animal[] animals = {dog1,dog2,dog3,dog4,cat,duck1,duck2};
        for(int i = 0;i<animals.length;i++){
            System.out.println();
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println();
        }
       
    }
}
